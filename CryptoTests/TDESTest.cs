﻿using NUnit.Framework;
using CryptoServiceProviders;

namespace CryptoTests
{
    class TDESTest
    {
        TDESProvider tdesprovider = new TDESProvider("myTripleDesKeyForCryptog");


        [Test]
        public void CanGenerateKeys() {

            
            tdesprovider.GenerateKeys();

            Assert.AreEqual("myTriple", tdesprovider.tdes1);
            Assert.AreEqual("DesKeyFo", tdesprovider.tdes2);
            Assert.AreEqual("rCryptog", tdesprovider.tdes3);

        }

        [Test]
        public void CanGetKeys()
        {
            tdesprovider.GenerateKeys();
            var keys = tdesprovider.GetKeys();
            

            Assert.AreEqual(3, keys.Count);


        }

    }
}
