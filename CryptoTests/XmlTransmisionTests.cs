﻿using NUnit.Framework;
using TransmisionHandling;
using Entities;

namespace CryptoTests
{
    class XmlTransmisionTests
    {
        
        [Test]
        public void CanExportTdesToXml()
        {

            XmlTransmision xmlT = new XmlTransmision();

            var encryptor = xmlT.tdesEncryptor.encryptor;

            var answer = xmlT.WriteOutput(new TdesInfo(encryptor.tdes1, encryptor.tdes2, encryptor.tdes3, ""));

            Assert.True(true);

        }

        [Test]
        public void CanExportRsaToXml()
        {

            XmlTransmision xmlT = new XmlTransmision();

            var encryptor = xmlT.rsaEncryptor.encryptor;

            var answer = xmlT.WriteOutput(new RsaInfo(encryptor.privateKey, encryptor.publicKey));

            Assert.True(true);

        }

        [Test]
        public void CanExportMessageToXml()
        {

            XmlTransmision xmlT = new XmlTransmision();

            var answer = xmlT.WriteOutput(new MessageInfo("Message to be transported"));

            Assert.True(true);

        }

        [Test]
        public void CanReadRsaKeyFromXml()
        {

            XmlTransmision xmlT = new XmlTransmision();

            IDataTransmision infoObject = xmlT.LoadData("C:\\cryptostudy\\XmlFiles\\cp_esclavo.xml");
            RsaInfo explicitObject = (RsaInfo)infoObject;
            string keyInXmlFile = "<RSAKeyValue><Modulus>oseXwIsUcVbe/F3WVCUa5haUtSthtbVFiyUQtmuvkJzWsks/lL8KMitDwGrg5EfDxEhk46WuXNaFOzNKHOWE9NCOvmNz4icNI5hGeh1oy1zL1Qnz9VE1kaSBEHuxftnmUSISxfAT5re4HI9nW/PwK0Ru9xr32D3pATXfaMc+bEk=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

            Assert.AreEqual(keyInXmlFile,explicitObject.publicKey);

        }

        [Test]
        public void CanReadTdesKeysFromXml()
        {

            XmlTransmision xmlT = new XmlTransmision();

            IDataTransmision infoObject = xmlT.LoadData("C:\\cryptostudy\\XmlFiles\\tdesencriptado.xml");
            TdesInfo explicitObject = (TdesInfo)infoObject;
            string keyInXmlFile = "KGDkqL3Oi5byBQOBfsfERrMNSXYPUKt1iv0rRVYqERnWw5EIwdvSAZvGOwDbFE281uuq+aBIBZ1fYngxi2TNgaZViPvaa1o33JfP7PqDaIECiy2GaZAzDGl0LmdlZr5DvvrynIYtMQ7dYnRg+CCx0buoZwQTydQebJDl2jFY6V8=";

            Assert.AreEqual(keyInXmlFile, explicitObject.tdes2);

        }

        [Test]
        public void CanReadEncryptedMessageFromXml()
        {

            XmlTransmision xmlT = new XmlTransmision();

            IDataTransmision infoObject = xmlT.LoadData("C:\\cryptostudy\\XmlFiles\\textoencriptado.xml");
            MessageInfo explicitObject = (MessageInfo)infoObject;
            string messageInXmlFile = "zbZ9/zGnG9fhtMfeul7vMthkBTdCenAffcXesJmnVG8=";

            Assert.AreEqual(messageInXmlFile, explicitObject.message);

        }

    }
}
