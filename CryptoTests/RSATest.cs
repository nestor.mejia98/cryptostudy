﻿using NUnit.Framework;
using CryptoServiceProviders;

namespace CryptoTests
{
    public class RSATest
    {

        [Test]
        public void CanGenerateRSAPublicKey() {

            var rsap = new RSAProvider();

            rsap.GeneratePublicKey();

            string pubkey = rsap.publicKey;

            Assert.IsNotEmpty(pubkey);

        }

        [Test]
        public void CanGenerateRSAPrivateKey()
        {

            var rsap = new RSAProvider();

            rsap.GeneratePrivateKey();

            string privkey = rsap.privateKey;

            Assert.IsNotEmpty(privkey);

        }

        [Test]
        public void CanGenerateBothRSAKeys()
        {

            var rsap = new RSAProvider();

            rsap.GenerateKeys();
            

            Assert.AreNotEqual(rsap.publicKey, rsap.privateKey);

        }

        [Test]
        public void CanGetKeys()
        {

            var rsap = new RSAProvider();

            rsap.GenerateKeys();

            var keys = rsap.GetKeys();


            Assert.AreEqual(2, keys.Count);

        }

    }
}
