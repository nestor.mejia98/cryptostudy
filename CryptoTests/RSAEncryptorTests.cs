﻿using NUnit.Framework;
using EncryptionService;

namespace CryptoTests
{
    class RSAEncryptorTests
    {

        RSAencryptor encryptor = new RSAencryptor();

        [Test]
        public void CanEncryptText() {

            string plainText = "RSA encryptor test";

            string cypherText = encryptor.EncryptText(plainText, encryptor.encryptor.publicKey);

            Assert.AreNotEqual(plainText, cypherText);

        }

        [Test]
        public void CanDecryptText() {

            string plainText = "RSA decryptor test";

            string cypherText = encryptor.EncryptText(plainText, encryptor.encryptor.publicKey) ;

            var decryptedText = encryptor.DecryptText(cypherText);

            Assert.AreEqual("RSA decryptor test", decryptedText);

        }

    }
}
