﻿using EncryptionService;
using NUnit.Framework;

namespace CryptoTests
{
    class TDESEncryptorTests
    {
        TDESEncryptor encryptor = new TDESEncryptor();

        [Test]
        public void CanEncryptText()
        {

            string plainText = "TDES encryption test";

            string cypherText = encryptor.EncryptText(plainText, "");

            Assert.AreNotEqual(plainText, cypherText);

        }

        [Test]
        public void CanDecryptText()
        {

            string plainText = "TDES decryptor test";

            //string cypherText = encryptor.EncryptText(plainText);
            string cypherText = "cb5sCxGHBiA5tQdOpIqXgsXqTUKrznoN";


            var decryptedText = encryptor.DecryptText(cypherText);

            Assert.AreEqual(plainText, decryptedText);

        }

    }
}
