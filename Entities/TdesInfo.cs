﻿namespace Entities
{
    public class TdesInfo : IDataTransmision
    {

        public string tdes1, tdes2, tdes3, iv;

        public TdesInfo(string k1, string k2, string k3, string iv) {
            tdes1 = k1;
            tdes2 = k2;
            tdes3 = k3;
            this.iv = iv;
        }

        public void SetIv(string iv) {

            this.iv = iv;

        }

    }
}
