﻿namespace Entities
{
    public class RsaInfo : IDataTransmision
    {

        public string privateKey, publicKey;

        public RsaInfo(string priv, string pub) {

            privateKey = priv;
            publicKey = pub;

        }

    }
}
