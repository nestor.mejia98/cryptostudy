﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Entities;
using Connection;

namespace FrontEnd
{
    public partial class Master : Form
    {

        Slave slaveRef;
        Operation op;
        private string Tdes1;
        private string Tdes2;
        private string Tdes3;
        private string Iv;
        private string EncryptedTdes1;
        private string EncryptedTdes2;
        private string EncryptedTdes3;
        private string EncryptedIv;

        public Master(Operation o)
        {
            op = o;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(50, 75);
            InitializeComponent();
        }

        public void SetSlaveRef(Slave s) {

            this.slaveRef = s;

        }

        private void ImportRsaPubKeyFromSlave(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = op.GetRuntimePath(),
                Title = "Seleccionar fichero con clave pública RSA del esclavo",
                DefaultExt = "xml",
                CheckFileExists = true,
                CheckPathExists = true,
                Filter = "xml files (*.xml)|*.xml"
        };
            openFileDialog1.ShowDialog();
            string path = openFileDialog1.FileName;
            string file = Path.GetFileName(path);
            if (file.Equals("cp_esclavo.xml"))
            {
                IDataTransmision temp = op.LoadDataFromXml(path);
                RsaInfo casted = (RsaInfo)temp;
                MasterPubKeyValueFromSlave.Text = casted.publicKey;
            }
            else {

                MessageBox.Show("Por favor selecciona el archivo de la clave pública: cp_esclavo.xml", "Fichero incorrecto");
                ImportRsaPubKeyFromSlave(sender, e);

            }
            
        }

        private void ImportMessageFromXml(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = op.GetRuntimePath(),
                Title = "Seleccionar fichero con mensaje encriptado",
                DefaultExt = "xml",
                CheckFileExists = true,
                CheckPathExists = true,
                Filter = "xml files (*.xml)|*.xml"
            };
            openFileDialog1.ShowDialog();
            string path = openFileDialog1.FileName;
            string file = Path.GetFileName(path);
            if (file.Equals("textoencriptado.xml"))
            {
                IDataTransmision temp = op.LoadDataFromXml(path);
                MessageInfo casted = (MessageInfo)temp;
                MasterEncryptedMessageFromXmlLbl.Text = casted.message;
            }
            else
            {

                MessageBox.Show("Por favor selecciona el archivo del mensaje encriptado: textoencriptado.xml", "Fichero incorrecto");
                ImportMessageFromXml(sender, e);

            }
        }

        private void GenerateRsaKeys(object sender, EventArgs e)
        {
            IList<string> rsaKeys = op.GetRsaKeys();

            string pubKey = rsaKeys.ElementAt(1);
            string privKey = rsaKeys.ElementAt(0);

            MasterPrivKeyValueLbl.Text = privKey;
            MasterPubKeyValueLbl.Text = pubKey;
            
            slaveRef.SlavePubKeyValueLbl.Text = pubKey;
            slaveRef.SlavePrivKeyValueLbl.Text = privKey;
        }

        private void GenerateTdesKeys(object sender, EventArgs e)
        {
            IList<string> tdesKeys = op.GetTdesKeys();
            Tdes1 = tdesKeys.ElementAt(0);
            
            Tdes2 = tdesKeys.ElementAt(1);
            
            Tdes3 = tdesKeys.ElementAt(2);

            MasterTdesKeyValueLbl.Text = Tdes1 + Tdes2 + Tdes3;

            var hexStrings = op.GetSplitTdesKeys();
            
            Tdes1 = hexStrings.ElementAt(0);
           
            Tdes2 = hexStrings.ElementAt(1);

            Tdes3 = hexStrings.ElementAt(2);

            Iv = hexStrings.ElementAt(3);
        }

        private void EncryptTdesKeyWithRsa(object sender, EventArgs e)
        {
            string rsaKey = MasterPubKeyValueFromSlave.Text;

            var EncryptedTdesInfo = op.EncryptTdesWithRsa(Tdes1, Tdes2, Tdes3, Iv, rsaKey);

            EncryptedTdes1 = EncryptedTdesInfo.ElementAt(0);
            EncryptedTdes2 = EncryptedTdesInfo.ElementAt(1);
            EncryptedTdes3 = EncryptedTdesInfo.ElementAt(2);
            EncryptedIv = EncryptedTdesInfo.ElementAt(3);
            MasterTdesEncryptedValueLbl.Text = EncryptedTdes1+EncryptedTdes2+EncryptedTdes3;
        }

        private void ExportEncryptedTdesToXml(object sender, EventArgs e)
        {
            string writenToXml = op.WriteInfoToXml(new TdesInfo(EncryptedTdes1, EncryptedTdes2, EncryptedTdes3, EncryptedIv));

                MessageBox.Show("Valor: " + writenToXml, "Claves TDES exportadas con éxito");

       
        }

        private void DecryptTextWithTdes(object sender, EventArgs e)
        {
            MasterDecryptedTextValueLbl.Text = op.DecryptTextWithTdes(MasterEncryptedMessageFromXmlLbl.Text);
        }

        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }
    }
}

