﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Entities;
using Connection;

namespace FrontEnd
{
    public partial class Slave : Form
    {
        Master masterRef;
        TdesInfo castedTdes;
        Operation op;

        public Slave(Master r, Operation o)
        {
            masterRef = r;
            op = o;
            this.StartPosition = FormStartPosition.Manual;
            this.Load += (s, ea) => {
                var wa = Screen.PrimaryScreen.WorkingArea;
                this.Location = new Point((wa.Right-100) - this.Width, (wa.Bottom-70) - this.Height);
            };
            InitializeComponent();
        }

        private void ImportTdesXml(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = op.GetRuntimePath(),
                Title = "Seleccionar fichero con claves TDES encriptadas",
                DefaultExt = "xml",
                CheckFileExists = true,
                CheckPathExists = true,
                Filter = "xml files (*.xml)|*.xml"
            };

            openFileDialog1.ShowDialog();

            string path = openFileDialog1.FileName;
            string file = Path.GetFileName(path);

            if (file.Equals("tdesencriptado.xml"))
            {
                IDataTransmision temp = op.LoadDataFromXml(path);
                castedTdes = (TdesInfo)temp;
                SlaveTdesEncryptedValueLbl.Text = castedTdes.tdes1+ castedTdes.tdes2+ castedTdes.tdes3;
            }
            else
            {

                MessageBox.Show("Por favor selecciona el archivo de las claves TDES: tdesencriptado.xml", "Fichero incorrecto");
                ImportTdesXml(sender, e);

            }

        }

        private void GenerateRsaKeys(object sender, EventArgs e)
        {
            IList<string> rsaKeys = op.GetRsaKeys();

            string pubKey = rsaKeys.ElementAt(1);
            string privKey = rsaKeys.ElementAt(0);

            SlavePrivKeyValueLbl.Text = privKey;
            SlavePubKeyValueLbl.Text = pubKey;

            masterRef.MasterPubKeyValueLbl.Text = pubKey;
            masterRef.MasterPrivKeyValueLbl.Text = privKey;

        }

        private void ExportPubKeyToXml(object sender, EventArgs e)
        {
            string writenToXml = op.WriteInfoToXml(new RsaInfo(SlavePrivKeyValueLbl.Text, SlavePubKeyValueLbl.Text));
            if (writenToXml.Equals(SlavePubKeyValueLbl.Text)) {

                MessageBox.Show("Valor: " + writenToXml,"Clave pública RSA exportada con éxito");

            }
               
        }

        private void DecryptTdesKey(object sender, EventArgs e)
        {

            var plainTdes = op.DecryptTdesInfo(castedTdes.tdes1, castedTdes.tdes2, castedTdes.tdes3, castedTdes.iv);

            SlaveTdesKeyValueLbl.Text = (string)plainTdes["PlainDecTdes1"] + plainTdes["PlainDecTdes2"] + plainTdes["PlainDecTdes3"];

        }

        private void EncryptTextWithTdes(object sender, EventArgs e)
        {
            string encryptedText = "";

            if (!SlaveTypedTextBox.Text.Equals(""))
            {

                encryptedText = op.EncryptMessageWithTdes(SlaveTypedTextBox.Text, "");
                SlaveEncryptedTextValueLbl.Text = encryptedText;

            }
            else {

                MessageBox.Show("Por favor introduce un texto", "Error");

            }
        }

        private void ExportEncryptedMessageToXml(object sender, EventArgs e)
        {
            string writtenToXml = op.WriteInfoToXml(new MessageInfo(SlaveEncryptedTextValueLbl.Text));
            if (writtenToXml.Equals(SlaveEncryptedTextValueLbl.Text)) {

                MessageBox.Show("Valor: " + writtenToXml, "Mensaje encriptado exportado con éxito");

            }
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}
