﻿namespace FrontEnd
{
    partial class Master
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Master));
            this.MasterLabel = new System.Windows.Forms.Label();
            this.MasterGenerateRsaButton = new System.Windows.Forms.Button();
            this.MasterPubKeyLabel = new System.Windows.Forms.Label();
            this.MasterPrivKeyLbl = new System.Windows.Forms.Label();
            this.MasterPubKeyValueLbl = new System.Windows.Forms.Label();
            this.MasterPrivKeyValueLbl = new System.Windows.Forms.Label();
            this.MasterImportPubKeyFromSlaveBtn = new System.Windows.Forms.Button();
            this.MasterPubKeyValueFromSlave = new System.Windows.Forms.Label();
            this.MasterGenerateTdesKeyBtn = new System.Windows.Forms.Button();
            this.MasterTdesKeyValueLbl = new System.Windows.Forms.Label();
            this.MasterEncryptTdesKeyBtn = new System.Windows.Forms.Button();
            this.MasterTdesEncryptedValueLbl = new System.Windows.Forms.Label();
            this.MasterExportEncryptedTdesXml = new System.Windows.Forms.Button();
            this.MasterImportMessageFromXml = new System.Windows.Forms.Button();
            this.MasterEncryptedMessageFromXmlLbl = new System.Windows.Forms.Label();
            this.MasterDecryptTextWithTdesBtn = new System.Windows.Forms.Button();
            this.MasterDecryptedTextValueLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // MasterLabel
            // 
            this.MasterLabel.AutoSize = true;
            this.MasterLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MasterLabel.Location = new System.Drawing.Point(182, 9);
            this.MasterLabel.Name = "MasterLabel";
            this.MasterLabel.Size = new System.Drawing.Size(185, 26);
            this.MasterLabel.TabIndex = 0;
            this.MasterLabel.Text = "SOY MAESTRO";
            this.MasterLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MasterGenerateRsaButton
            // 
            this.MasterGenerateRsaButton.Location = new System.Drawing.Point(12, 60);
            this.MasterGenerateRsaButton.Name = "MasterGenerateRsaButton";
            this.MasterGenerateRsaButton.Size = new System.Drawing.Size(127, 23);
            this.MasterGenerateRsaButton.TabIndex = 1;
            this.MasterGenerateRsaButton.Text = "Generar claves RSA";
            this.MasterGenerateRsaButton.UseVisualStyleBackColor = true;
            this.MasterGenerateRsaButton.Click += new System.EventHandler(this.GenerateRsaKeys);
            // 
            // MasterPubKeyLabel
            // 
            this.MasterPubKeyLabel.AutoSize = true;
            this.MasterPubKeyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MasterPubKeyLabel.Location = new System.Drawing.Point(168, 52);
            this.MasterPubKeyLabel.Name = "MasterPubKeyLabel";
            this.MasterPubKeyLabel.Size = new System.Drawing.Size(84, 13);
            this.MasterPubKeyLabel.TabIndex = 2;
            this.MasterPubKeyLabel.Text = "Clave pública";
            // 
            // MasterPrivKeyLbl
            // 
            this.MasterPrivKeyLbl.AutoSize = true;
            this.MasterPrivKeyLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MasterPrivKeyLbl.Location = new System.Drawing.Point(168, 79);
            this.MasterPrivKeyLbl.Name = "MasterPrivKeyLbl";
            this.MasterPrivKeyLbl.Size = new System.Drawing.Size(85, 13);
            this.MasterPrivKeyLbl.TabIndex = 3;
            this.MasterPrivKeyLbl.Text = "Clave privada";
            // 
            // MasterPubKeyValueLbl
            // 
            this.MasterPubKeyValueLbl.AutoSize = true;
            this.MasterPubKeyValueLbl.Location = new System.Drawing.Point(318, 52);
            this.MasterPubKeyValueLbl.Name = "MasterPubKeyValueLbl";
            this.MasterPubKeyValueLbl.Size = new System.Drawing.Size(97, 13);
            this.MasterPubKeyValueLbl.TabIndex = 4;
            this.MasterPubKeyValueLbl.Text = "<valor de la clave>";
            // 
            // MasterPrivKeyValueLbl
            // 
            this.MasterPrivKeyValueLbl.AutoSize = true;
            this.MasterPrivKeyValueLbl.Location = new System.Drawing.Point(320, 79);
            this.MasterPrivKeyValueLbl.Name = "MasterPrivKeyValueLbl";
            this.MasterPrivKeyValueLbl.Size = new System.Drawing.Size(97, 13);
            this.MasterPrivKeyValueLbl.TabIndex = 5;
            this.MasterPrivKeyValueLbl.Text = "<valor de la clave>";
            // 
            // MasterImportPubKeyFromSlaveBtn
            // 
            this.MasterImportPubKeyFromSlaveBtn.Location = new System.Drawing.Point(13, 118);
            this.MasterImportPubKeyFromSlaveBtn.Name = "MasterImportPubKeyFromSlaveBtn";
            this.MasterImportPubKeyFromSlaveBtn.Size = new System.Drawing.Size(188, 23);
            this.MasterImportPubKeyFromSlaveBtn.TabIndex = 6;
            this.MasterImportPubKeyFromSlaveBtn.Text = "Importar clave pública RSA esclavo";
            this.MasterImportPubKeyFromSlaveBtn.UseVisualStyleBackColor = true;
            this.MasterImportPubKeyFromSlaveBtn.Click += new System.EventHandler(this.ImportRsaPubKeyFromSlave);
            // 
            // MasterPubKeyValueFromSlave
            // 
            this.MasterPubKeyValueFromSlave.AutoSize = true;
            this.MasterPubKeyValueFromSlave.Location = new System.Drawing.Point(266, 122);
            this.MasterPubKeyValueFromSlave.Name = "MasterPubKeyValueFromSlave";
            this.MasterPubKeyValueFromSlave.Size = new System.Drawing.Size(174, 13);
            this.MasterPubKeyValueFromSlave.TabIndex = 7;
            this.MasterPubKeyValueFromSlave.Text = "<valor de la clave pública esclavo>";
            // 
            // MasterGenerateTdesKeyBtn
            // 
            this.MasterGenerateTdesKeyBtn.Location = new System.Drawing.Point(13, 174);
            this.MasterGenerateTdesKeyBtn.Name = "MasterGenerateTdesKeyBtn";
            this.MasterGenerateTdesKeyBtn.Size = new System.Drawing.Size(126, 23);
            this.MasterGenerateTdesKeyBtn.TabIndex = 8;
            this.MasterGenerateTdesKeyBtn.Text = "Generar clave TDES";
            this.MasterGenerateTdesKeyBtn.UseVisualStyleBackColor = true;
            this.MasterGenerateTdesKeyBtn.Click += new System.EventHandler(this.GenerateTdesKeys);
            // 
            // MasterTdesKeyValueLbl
            // 
            this.MasterTdesKeyValueLbl.AutoSize = true;
            this.MasterTdesKeyValueLbl.Location = new System.Drawing.Point(171, 178);
            this.MasterTdesKeyValueLbl.Name = "MasterTdesKeyValueLbl";
            this.MasterTdesKeyValueLbl.Size = new System.Drawing.Size(129, 13);
            this.MasterTdesKeyValueLbl.TabIndex = 9;
            this.MasterTdesKeyValueLbl.Text = "<valor de la clave TDES>";
            // 
            // MasterEncryptTdesKeyBtn
            // 
            this.MasterEncryptTdesKeyBtn.Location = new System.Drawing.Point(13, 229);
            this.MasterEncryptTdesKeyBtn.Name = "MasterEncryptTdesKeyBtn";
            this.MasterEncryptTdesKeyBtn.Size = new System.Drawing.Size(175, 45);
            this.MasterEncryptTdesKeyBtn.TabIndex = 10;
            this.MasterEncryptTdesKeyBtn.Text = "Encriptar clave TDES con RSA y clave pública esclavo";
            this.MasterEncryptTdesKeyBtn.UseVisualStyleBackColor = true;
            this.MasterEncryptTdesKeyBtn.Click += new System.EventHandler(this.EncryptTdesKeyWithRsa);
            // 
            // MasterTdesEncryptedValueLbl
            // 
            this.MasterTdesEncryptedValueLbl.AutoSize = true;
            this.MasterTdesEncryptedValueLbl.Location = new System.Drawing.Point(203, 245);
            this.MasterTdesEncryptedValueLbl.Name = "MasterTdesEncryptedValueLbl";
            this.MasterTdesEncryptedValueLbl.Size = new System.Drawing.Size(182, 13);
            this.MasterTdesEncryptedValueLbl.TabIndex = 11;
            this.MasterTdesEncryptedValueLbl.Text = "<valor de la clave TDES encriptada>";
            // 
            // MasterExportEncryptedTdesXml
            // 
            this.MasterExportEncryptedTdesXml.Location = new System.Drawing.Point(424, 225);
            this.MasterExportEncryptedTdesXml.Name = "MasterExportEncryptedTdesXml";
            this.MasterExportEncryptedTdesXml.Size = new System.Drawing.Size(75, 57);
            this.MasterExportEncryptedTdesXml.TabIndex = 12;
            this.MasterExportEncryptedTdesXml.Text = "Exportar xml TDES encriptada";
            this.MasterExportEncryptedTdesXml.UseVisualStyleBackColor = true;
            this.MasterExportEncryptedTdesXml.Click += new System.EventHandler(this.ExportEncryptedTdesToXml);
            // 
            // MasterImportMessageFromXml
            // 
            this.MasterImportMessageFromXml.Location = new System.Drawing.Point(13, 312);
            this.MasterImportMessageFromXml.Name = "MasterImportMessageFromXml";
            this.MasterImportMessageFromXml.Size = new System.Drawing.Size(135, 23);
            this.MasterImportMessageFromXml.TabIndex = 13;
            this.MasterImportMessageFromXml.Text = "Importar mensaje de xml";
            this.MasterImportMessageFromXml.UseVisualStyleBackColor = true;
            this.MasterImportMessageFromXml.Click += new System.EventHandler(this.ImportMessageFromXml);
            // 
            // MasterEncryptedMessageFromXmlLbl
            // 
            this.MasterEncryptedMessageFromXmlLbl.AutoSize = true;
            this.MasterEncryptedMessageFromXmlLbl.Location = new System.Drawing.Point(203, 318);
            this.MasterEncryptedMessageFromXmlLbl.Name = "MasterEncryptedMessageFromXmlLbl";
            this.MasterEncryptedMessageFromXmlLbl.Size = new System.Drawing.Size(165, 13);
            this.MasterEncryptedMessageFromXmlLbl.TabIndex = 14;
            this.MasterEncryptedMessageFromXmlLbl.Text = "<texto encriptado del fichero xml>";
            // 
            // MasterDecryptTextWithTdesBtn
            // 
            this.MasterDecryptTextWithTdesBtn.Location = new System.Drawing.Point(13, 371);
            this.MasterDecryptTextWithTdesBtn.Name = "MasterDecryptTextWithTdesBtn";
            this.MasterDecryptTextWithTdesBtn.Size = new System.Drawing.Size(279, 23);
            this.MasterDecryptTextWithTdesBtn.TabIndex = 15;
            this.MasterDecryptTextWithTdesBtn.Text = "Desencriptar texto con TDES y clave TDES creada";
            this.MasterDecryptTextWithTdesBtn.UseVisualStyleBackColor = true;
            this.MasterDecryptTextWithTdesBtn.Click += new System.EventHandler(this.DecryptTextWithTdes);
            // 
            // MasterDecryptedTextValueLbl
            // 
            this.MasterDecryptedTextValueLbl.AutoSize = true;
            this.MasterDecryptedTextValueLbl.Location = new System.Drawing.Point(13, 424);
            this.MasterDecryptedTextValueLbl.Name = "MasterDecryptedTextValueLbl";
            this.MasterDecryptedTextValueLbl.Size = new System.Drawing.Size(112, 13);
            this.MasterDecryptedTextValueLbl.TabIndex = 16;
            this.MasterDecryptedTextValueLbl.Text = "<texto desencriptado>";
            this.MasterDecryptedTextValueLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Master
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(533, 591);
            this.Controls.Add(this.MasterDecryptedTextValueLbl);
            this.Controls.Add(this.MasterDecryptTextWithTdesBtn);
            this.Controls.Add(this.MasterEncryptedMessageFromXmlLbl);
            this.Controls.Add(this.MasterImportMessageFromXml);
            this.Controls.Add(this.MasterExportEncryptedTdesXml);
            this.Controls.Add(this.MasterTdesEncryptedValueLbl);
            this.Controls.Add(this.MasterEncryptTdesKeyBtn);
            this.Controls.Add(this.MasterTdesKeyValueLbl);
            this.Controls.Add(this.MasterGenerateTdesKeyBtn);
            this.Controls.Add(this.MasterPubKeyValueFromSlave);
            this.Controls.Add(this.MasterImportPubKeyFromSlaveBtn);
            this.Controls.Add(this.MasterPrivKeyValueLbl);
            this.Controls.Add(this.MasterPubKeyValueLbl);
            this.Controls.Add(this.MasterPrivKeyLbl);
            this.Controls.Add(this.MasterPubKeyLabel);
            this.Controls.Add(this.MasterGenerateRsaButton);
            this.Controls.Add(this.MasterLabel);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Master";
            this.Text = "Master";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MasterLabel;
        private System.Windows.Forms.Button MasterGenerateRsaButton;
        private System.Windows.Forms.Label MasterPubKeyLabel;
        private System.Windows.Forms.Label MasterPrivKeyLbl;
        private System.Windows.Forms.Button MasterImportPubKeyFromSlaveBtn;
        private System.Windows.Forms.Label MasterPubKeyValueFromSlave;
        private System.Windows.Forms.Button MasterGenerateTdesKeyBtn;
        private System.Windows.Forms.Label MasterTdesKeyValueLbl;
        private System.Windows.Forms.Button MasterEncryptTdesKeyBtn;
        private System.Windows.Forms.Label MasterTdesEncryptedValueLbl;
        private System.Windows.Forms.Button MasterExportEncryptedTdesXml;
        private System.Windows.Forms.Button MasterImportMessageFromXml;
        private System.Windows.Forms.Label MasterEncryptedMessageFromXmlLbl;
        private System.Windows.Forms.Button MasterDecryptTextWithTdesBtn;
        private System.Windows.Forms.Label MasterDecryptedTextValueLbl;
        public System.Windows.Forms.Label MasterPubKeyValueLbl;
        public System.Windows.Forms.Label MasterPrivKeyValueLbl;
    }
}

