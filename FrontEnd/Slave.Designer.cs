﻿namespace FrontEnd
{
    partial class Slave
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Slave));
            this.SlaveLabel = new System.Windows.Forms.Label();
            this.SlaveGenerateRsaBtn = new System.Windows.Forms.Button();
            this.SlavePubKeyLbl = new System.Windows.Forms.Label();
            this.SlavePrivKeyLbl = new System.Windows.Forms.Label();
            this.SlavePubKeyValueLbl = new System.Windows.Forms.Label();
            this.SlavePrivKeyValueLbl = new System.Windows.Forms.Label();
            this.SlaveExportRsaPubKeyXmlBtn = new System.Windows.Forms.Button();
            this.SlaveImportTdesKeyFromXmlBtn = new System.Windows.Forms.Button();
            this.SlaveTdesEncryptedValueLbl = new System.Windows.Forms.Label();
            this.SlaveDecryptTdesKeysBtn = new System.Windows.Forms.Button();
            this.SlaveTdesKeyValueLbl = new System.Windows.Forms.Label();
            this.SlaveTypeTextLbl = new System.Windows.Forms.Label();
            this.SlaveTypedTextBox = new System.Windows.Forms.TextBox();
            this.SlaveEncryptTextWithTdesBtn = new System.Windows.Forms.Button();
            this.SlaveEncryptedTextValueLbl = new System.Windows.Forms.Label();
            this.SlaveExportMessageToXmlBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SlaveLabel
            // 
            this.SlaveLabel.AutoSize = true;
            this.SlaveLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SlaveLabel.Location = new System.Drawing.Point(154, 9);
            this.SlaveLabel.Name = "SlaveLabel";
            this.SlaveLabel.Size = new System.Drawing.Size(182, 26);
            this.SlaveLabel.TabIndex = 0;
            this.SlaveLabel.Text = "SOY ESCLAVO";
            // 
            // SlaveGenerateRsaBtn
            // 
            this.SlaveGenerateRsaBtn.Location = new System.Drawing.Point(29, 65);
            this.SlaveGenerateRsaBtn.Name = "SlaveGenerateRsaBtn";
            this.SlaveGenerateRsaBtn.Size = new System.Drawing.Size(122, 23);
            this.SlaveGenerateRsaBtn.TabIndex = 1;
            this.SlaveGenerateRsaBtn.Text = "Generar claves RSA";
            this.SlaveGenerateRsaBtn.UseVisualStyleBackColor = true;
            this.SlaveGenerateRsaBtn.Click += new System.EventHandler(this.GenerateRsaKeys);
            // 
            // SlavePubKeyLbl
            // 
            this.SlavePubKeyLbl.AutoSize = true;
            this.SlavePubKeyLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SlavePubKeyLbl.Location = new System.Drawing.Point(169, 54);
            this.SlavePubKeyLbl.Name = "SlavePubKeyLbl";
            this.SlavePubKeyLbl.Size = new System.Drawing.Size(84, 13);
            this.SlavePubKeyLbl.TabIndex = 2;
            this.SlavePubKeyLbl.Text = "Clave pública";
            // 
            // SlavePrivKeyLbl
            // 
            this.SlavePrivKeyLbl.AutoSize = true;
            this.SlavePrivKeyLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SlavePrivKeyLbl.Location = new System.Drawing.Point(172, 86);
            this.SlavePrivKeyLbl.Name = "SlavePrivKeyLbl";
            this.SlavePrivKeyLbl.Size = new System.Drawing.Size(85, 13);
            this.SlavePrivKeyLbl.TabIndex = 3;
            this.SlavePrivKeyLbl.Text = "Clave privada";
            // 
            // SlavePubKeyValueLbl
            // 
            this.SlavePubKeyValueLbl.AutoSize = true;
            this.SlavePubKeyValueLbl.Location = new System.Drawing.Point(282, 54);
            this.SlavePubKeyValueLbl.Name = "SlavePubKeyValueLbl";
            this.SlavePubKeyValueLbl.Size = new System.Drawing.Size(97, 13);
            this.SlavePubKeyValueLbl.TabIndex = 4;
            this.SlavePubKeyValueLbl.Text = "<valor de la clave>";
            // 
            // SlavePrivKeyValueLbl
            // 
            this.SlavePrivKeyValueLbl.AutoSize = true;
            this.SlavePrivKeyValueLbl.Location = new System.Drawing.Point(281, 86);
            this.SlavePrivKeyValueLbl.Name = "SlavePrivKeyValueLbl";
            this.SlavePrivKeyValueLbl.Size = new System.Drawing.Size(97, 13);
            this.SlavePrivKeyValueLbl.TabIndex = 5;
            this.SlavePrivKeyValueLbl.Text = "<valor de la clave>";
            // 
            // SlaveExportRsaPubKeyXmlBtn
            // 
            this.SlaveExportRsaPubKeyXmlBtn.Location = new System.Drawing.Point(412, 59);
            this.SlaveExportRsaPubKeyXmlBtn.Name = "SlaveExportRsaPubKeyXmlBtn";
            this.SlaveExportRsaPubKeyXmlBtn.Size = new System.Drawing.Size(80, 34);
            this.SlaveExportRsaPubKeyXmlBtn.TabIndex = 6;
            this.SlaveExportRsaPubKeyXmlBtn.Text = "Exportar xml clave pública";
            this.SlaveExportRsaPubKeyXmlBtn.UseVisualStyleBackColor = true;
            this.SlaveExportRsaPubKeyXmlBtn.Click += new System.EventHandler(this.ExportPubKeyToXml);
            // 
            // SlaveImportTdesKeyFromXmlBtn
            // 
            this.SlaveImportTdesKeyFromXmlBtn.Location = new System.Drawing.Point(29, 142);
            this.SlaveImportTdesKeyFromXmlBtn.Name = "SlaveImportTdesKeyFromXmlBtn";
            this.SlaveImportTdesKeyFromXmlBtn.Size = new System.Drawing.Size(139, 40);
            this.SlaveImportTdesKeyFromXmlBtn.TabIndex = 7;
            this.SlaveImportTdesKeyFromXmlBtn.Text = "Importar clave TDES de fichero xml";
            this.SlaveImportTdesKeyFromXmlBtn.UseVisualStyleBackColor = true;
            this.SlaveImportTdesKeyFromXmlBtn.Click += new System.EventHandler(this.ImportTdesXml);
            // 
            // SlaveTdesEncryptedValueLbl
            // 
            this.SlaveTdesEncryptedValueLbl.AutoSize = true;
            this.SlaveTdesEncryptedValueLbl.Location = new System.Drawing.Point(221, 156);
            this.SlaveTdesEncryptedValueLbl.Name = "SlaveTdesEncryptedValueLbl";
            this.SlaveTdesEncryptedValueLbl.Size = new System.Drawing.Size(182, 13);
            this.SlaveTdesEncryptedValueLbl.TabIndex = 8;
            this.SlaveTdesEncryptedValueLbl.Text = "<valor de la clave TDES encriptada>";
            // 
            // SlaveDecryptTdesKeysBtn
            // 
            this.SlaveDecryptTdesKeysBtn.Location = new System.Drawing.Point(29, 211);
            this.SlaveDecryptTdesKeysBtn.Name = "SlaveDecryptTdesKeysBtn";
            this.SlaveDecryptTdesKeysBtn.Size = new System.Drawing.Size(166, 23);
            this.SlaveDecryptTdesKeysBtn.TabIndex = 9;
            this.SlaveDecryptTdesKeysBtn.Text = "Desencriptar TDES de fichero";
            this.SlaveDecryptTdesKeysBtn.UseVisualStyleBackColor = true;
            this.SlaveDecryptTdesKeysBtn.Click += new System.EventHandler(this.DecryptTdesKey);
            // 
            // SlaveTdesKeyValueLbl
            // 
            this.SlaveTdesKeyValueLbl.AutoSize = true;
            this.SlaveTdesKeyValueLbl.Location = new System.Drawing.Point(224, 215);
            this.SlaveTdesKeyValueLbl.Name = "SlaveTdesKeyValueLbl";
            this.SlaveTdesKeyValueLbl.Size = new System.Drawing.Size(199, 13);
            this.SlaveTdesKeyValueLbl.TabIndex = 10;
            this.SlaveTdesKeyValueLbl.Text = "<valor de la clave TDES desencriptada>";
            // 
            // SlaveTypeTextLbl
            // 
            this.SlaveTypeTextLbl.AutoSize = true;
            this.SlaveTypeTextLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SlaveTypeTextLbl.Location = new System.Drawing.Point(29, 275);
            this.SlaveTypeTextLbl.Name = "SlaveTypeTextLbl";
            this.SlaveTypeTextLbl.Size = new System.Drawing.Size(97, 13);
            this.SlaveTypeTextLbl.TabIndex = 11;
            this.SlaveTypeTextLbl.Text = "Introducir texto:";
            // 
            // SlaveTypedTextBox
            // 
            this.SlaveTypedTextBox.Location = new System.Drawing.Point(157, 272);
            this.SlaveTypedTextBox.Name = "SlaveTypedTextBox";
            this.SlaveTypedTextBox.Size = new System.Drawing.Size(288, 20);
            this.SlaveTypedTextBox.TabIndex = 12;
            // 
            // SlaveEncryptTextWithTdesBtn
            // 
            this.SlaveEncryptTextWithTdesBtn.Location = new System.Drawing.Point(32, 327);
            this.SlaveEncryptTextWithTdesBtn.Name = "SlaveEncryptTextWithTdesBtn";
            this.SlaveEncryptTextWithTdesBtn.Size = new System.Drawing.Size(322, 23);
            this.SlaveEncryptTextWithTdesBtn.TabIndex = 13;
            this.SlaveEncryptTextWithTdesBtn.Text = "Encriptar texto algoritmo TDES y clave TDES desencriptada";
            this.SlaveEncryptTextWithTdesBtn.UseVisualStyleBackColor = true;
            this.SlaveEncryptTextWithTdesBtn.Click += new System.EventHandler(this.EncryptTextWithTdes);
            // 
            // SlaveEncryptedTextValueLbl
            // 
            this.SlaveEncryptedTextValueLbl.AutoSize = true;
            this.SlaveEncryptedTextValueLbl.Location = new System.Drawing.Point(32, 387);
            this.SlaveEncryptedTextValueLbl.Name = "SlaveEncryptedTextValueLbl";
            this.SlaveEncryptedTextValueLbl.Size = new System.Drawing.Size(163, 13);
            this.SlaveEncryptedTextValueLbl.TabIndex = 14;
            this.SlaveEncryptedTextValueLbl.Text = "<Resultado del texto encriptado>";
            // 
            // SlaveExportMessageToXmlBtn
            // 
            this.SlaveExportMessageToXmlBtn.Location = new System.Drawing.Point(227, 381);
            this.SlaveExportMessageToXmlBtn.Name = "SlaveExportMessageToXmlBtn";
            this.SlaveExportMessageToXmlBtn.Size = new System.Drawing.Size(144, 23);
            this.SlaveExportMessageToXmlBtn.TabIndex = 15;
            this.SlaveExportMessageToXmlBtn.Text = "Exportar mensaje a xml";
            this.SlaveExportMessageToXmlBtn.UseVisualStyleBackColor = true;
            this.SlaveExportMessageToXmlBtn.Click += new System.EventHandler(this.ExportEncryptedMessageToXml);
            // 
            // Slave
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(520, 586);
            this.Controls.Add(this.SlaveExportMessageToXmlBtn);
            this.Controls.Add(this.SlaveEncryptedTextValueLbl);
            this.Controls.Add(this.SlaveEncryptTextWithTdesBtn);
            this.Controls.Add(this.SlaveTypedTextBox);
            this.Controls.Add(this.SlaveTypeTextLbl);
            this.Controls.Add(this.SlaveTdesKeyValueLbl);
            this.Controls.Add(this.SlaveDecryptTdesKeysBtn);
            this.Controls.Add(this.SlaveTdesEncryptedValueLbl);
            this.Controls.Add(this.SlaveImportTdesKeyFromXmlBtn);
            this.Controls.Add(this.SlaveExportRsaPubKeyXmlBtn);
            this.Controls.Add(this.SlavePrivKeyValueLbl);
            this.Controls.Add(this.SlavePubKeyValueLbl);
            this.Controls.Add(this.SlavePrivKeyLbl);
            this.Controls.Add(this.SlavePubKeyLbl);
            this.Controls.Add(this.SlaveGenerateRsaBtn);
            this.Controls.Add(this.SlaveLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(200, 0);
            this.Name = "Slave";
            this.Text = "Slave";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label SlaveLabel;
        private System.Windows.Forms.Button SlaveGenerateRsaBtn;
        private System.Windows.Forms.Label SlavePubKeyLbl;
        private System.Windows.Forms.Label SlavePrivKeyLbl;
        private System.Windows.Forms.Button SlaveExportRsaPubKeyXmlBtn;
        private System.Windows.Forms.Button SlaveImportTdesKeyFromXmlBtn;
        private System.Windows.Forms.Label SlaveTdesEncryptedValueLbl;
        private System.Windows.Forms.Button SlaveDecryptTdesKeysBtn;
        private System.Windows.Forms.Label SlaveTdesKeyValueLbl;
        private System.Windows.Forms.Label SlaveTypeTextLbl;
        private System.Windows.Forms.TextBox SlaveTypedTextBox;
        private System.Windows.Forms.Button SlaveEncryptTextWithTdesBtn;
        private System.Windows.Forms.Label SlaveEncryptedTextValueLbl;
        private System.Windows.Forms.Button SlaveExportMessageToXmlBtn;
        public System.Windows.Forms.Label SlavePubKeyValueLbl;
        public System.Windows.Forms.Label SlavePrivKeyValueLbl;
    }
}