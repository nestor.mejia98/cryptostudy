﻿using System.Threading;
using System.Windows.Forms;

namespace FrontEnd
{
    //Clase para que el programa sea capaz de ejecutar ambos o un formulario a la vez
    public class MultiFormContext : ApplicationContext
    {
        private int openForms;
        public MultiFormContext(params Form[] forms)
        {
            openForms = forms.Length;

            foreach (var form in forms)
            {
                form.FormClosed += (s, args) =>
                {
                    // Cuando se cierren todos los formularios, terminar el programa
                    if (Interlocked.Decrement(ref openForms) == 0)
                        ExitThread();
                };

                form.Show();
            }
        }
    }
}
