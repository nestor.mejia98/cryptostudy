﻿using System;
using System.Windows.Forms;
using Connection;

namespace FrontEnd
{
    public static class Start
    {

        // Se inicia la aplicación
        [STAThread]
        public static void Main()
        {            

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            // Clase que maneja la comunicación con el Backend
            Operation operation = new Operation();

            // Seleccionar el rol para usar la aplicación
            var SelectRoles = new MyMessageBox();

            SelectRoles.ShowDialog();

            var result = SelectRoles.DialogResult;

            Master m = new Master(operation);
            Slave s = new Slave(m, operation);
            m.SetSlaveRef(s);
            Form[] FormsToExecute;

            // De acuerdo a qué ha seleccionado el usuario, creamos el array de formularios

            // Si seleccionó master
            if (result == DialogResult.OK)
            {

                FormsToExecute = new Form[1];
                FormsToExecute[0] = m;
            }
            // Si seleccionó ambos
            else if (result == DialogResult.Ignore)
            {

                FormsToExecute = new Form[2];
                FormsToExecute[0] = m;
                FormsToExecute[1] = s;

            }
            // Si seleccionó Esclavo
            else if (result == DialogResult.Yes)
            {

                FormsToExecute = new Form[1];
                FormsToExecute[0] = s;

            }
            // Si cierra el diálogo de selección, se termina el programa
            else {
                return;
            }

            // Se inicia la aplicación con el modo seleccionado
            Application.Run(new MultiFormContext(FormsToExecute));
        }
    }
}
