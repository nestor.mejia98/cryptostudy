﻿namespace FrontEnd
{
    partial class MyMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Master = new System.Windows.Forms.Button();
            this.Both = new System.Windows.Forms.Button();
            this.Slave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Master
            // 
            this.Master.Location = new System.Drawing.Point(12, 50);
            this.Master.Name = "Master";
            this.Master.Size = new System.Drawing.Size(75, 23);
            this.Master.TabIndex = 0;
            this.Master.Text = "Maestro";
            this.Master.UseVisualStyleBackColor = true;
            this.Master.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Master.Click += new System.EventHandler(this.MasterClicked);
            // 
            // Both
            // 
            this.Both.Location = new System.Drawing.Point(131, 50);
            this.Both.Name = "Both";
            this.Both.Size = new System.Drawing.Size(75, 23);
            this.Both.TabIndex = 1;
            this.Both.Text = "Ambos";
            this.Both.UseVisualStyleBackColor = true;
            this.Both.DialogResult = System.Windows.Forms.DialogResult.Ignore;
            // 
            // Slave
            // 
            this.Slave.Location = new System.Drawing.Point(245, 50);
            this.Slave.Name = "Slave";
            this.Slave.Size = new System.Drawing.Size(75, 23);
            this.Slave.TabIndex = 2;
            this.Slave.Text = "Esclavo";
            this.Slave.UseVisualStyleBackColor = true;
            this.Slave.DialogResult = System.Windows.Forms.DialogResult.Yes;
            // 
            // MyMessageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 117);
            this.Controls.Add(this.Slave);
            this.Controls.Add(this.Both);
            this.Controls.Add(this.Master);
            this.Name = "MyMessageBox";
            this.Text = "Selecciona el rol";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Master;
        private System.Windows.Forms.Button Both;
        private System.Windows.Forms.Button Slave;
    }
}