﻿using System;
using System.IO;
using System.Security.Cryptography;
using CryptoServiceProviders;

namespace EncryptionService
{
    public class TDESEncryptor : IEncryptor
    {

        public TDESProvider encryptor;

        public TDESEncryptor() {

            encryptor = new TDESProvider("MySuperSecretKey12345678");
            encryptor.GenerateKeys();

        }

        public string DecryptText(string cypherText)
        {

            encryptor.decTool = encryptor.csp.CreateDecryptor(encryptor.csp.Key, encryptor.csp.IV);

            string plainText = "";

            using (MemoryStream ms = new MemoryStream(StringToByteArray(cypherText)))
            {
                
                using (CryptoStream cs = new CryptoStream(ms, encryptor.decTool, CryptoStreamMode.Read))
                {
                     
                    using (StreamReader reader = new StreamReader(cs))
                        plainText = reader.ReadToEnd();
                }
            }

            return plainText;
        }

        public string EncryptText(string plainText, string pubKey)
        {

            encryptor.encTool = encryptor.csp.CreateEncryptor(encryptor.csp.Key, encryptor.csp.IV);

            byte[] encrypted;

            using (MemoryStream ms = new MemoryStream())
            {
               
                using (CryptoStream cs = new CryptoStream(ms, encryptor.encTool, CryptoStreamMode.Write))
                {
                     
                    using (StreamWriter sw = new StreamWriter(cs))
                        sw.Write(plainText);
                    encrypted = ms.ToArray();
                }
            }

            return ByteArrayToString(encrypted);
        }

        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}
