﻿using System;

namespace EncryptionService
{

    // Interfaz para las clases que se encargarán de encriptar y desencriptar los datos.
    // Se usan interfaces para, si los requerimientos cambian, se puedan usar distintos
    // algoritmos siempre y cuando implementen esta interfaz
    interface IEncryptor
    {

        String EncryptText(String plainText, string pubKey);
        String DecryptText(String cypherText);

    }
}
