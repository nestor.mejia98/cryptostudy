﻿using System;
using CryptoServiceProviders;

namespace EncryptionService
{
    public class RSAencryptor : IEncryptor
    {

        public RSAProvider encryptor;

        public RSAencryptor() {

            this.encryptor = new RSAProvider();
            encryptor.GenerateKeys();

        }

        public string DecryptText(string cypherText)
        {
            encryptor.csp.FromXmlString(encryptor.privateKey);

            var decryptedData = encryptor.csp.Decrypt(StringToByteArray(cypherText), true);

            return string.Join("", BitConverter.ToString(decryptedData).Split('-'));
        }

        public string EncryptText(string plainText, string pubKey)
        {

            string pubKeyToEncrypt;

            // Si el parámetro pubKey no es vacío, se usa ese como clave pública, si viene vacío,
            // se usa el de nuestro csp
            if (pubKey.Equals(""))
            {
                pubKeyToEncrypt = encryptor.publicKey;
            }
            else {

                pubKeyToEncrypt = pubKey;

            }

            encryptor.csp.FromXmlString(pubKeyToEncrypt);

            var data = StringToByteArray(plainText);

            var encryptedData = encryptor.csp.Encrypt(data, true);

            return ByteArrayToString(encryptedData);
        }

        // Función auxiliar que convierte un array de bytes a su representación en string hexadecimal
        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }

        // Función auxiliar que convierte una representación hexadecimal de un array de bytes, a un array de bytes
        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}
