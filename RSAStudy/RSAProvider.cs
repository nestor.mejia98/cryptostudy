﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace CryptoServiceProviders
{
    public class RSAProvider : ICryptoProvider
    {

        public string privateKey;
        public string publicKey;
        public RSACryptoServiceProvider csp = new RSACryptoServiceProvider(512);

        public RSAProvider() {

            GenerateKeys();

        }

        // La implementación RSA requiere generar clave pública y privada
        public void GenerateKeys()
        {
            GeneratePrivateKey();
            GeneratePublicKey();
            
        }

        public void GeneratePublicKey()
        {

            publicKey = csp.ToXmlString(false);  

        }

        public void GeneratePrivateKey() {

            privateKey = csp.ToXmlString(true);

        }

        public IList<String> GetKeys()
        {
            var keys = new List<String>
            {
                privateKey,
                publicKey,
              
            };

            return keys;
        }
    }
}
