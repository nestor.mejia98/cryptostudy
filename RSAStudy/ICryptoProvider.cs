﻿using System;
using System.Collections.Generic;

namespace CryptoServiceProviders
{
    // Interfaz para los proveedores de servicios criptográficos
    public interface ICryptoProvider
    {

        void GenerateKeys();
        IList<String> GetKeys();

    }
}
