﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace CryptoServiceProviders
{
    public class TDESProvider : ICryptoProvider
    {

        public TripleDESCryptoServiceProvider csp = new TripleDESCryptoServiceProvider();
        public string tdes1, tdes2, tdes3;
        public byte[] Tdes1, Tdes2, Tdes3;
        public ICryptoTransform encTool;
        public ICryptoTransform decTool;

        public TDESProvider(string key) {

            csp.GenerateKey();
            GenerateKeys();
            csp.GenerateIV();
        }

        //La implementación TDES requiere la división de la clave del csp en 3 byte array distintos
        public void GenerateKeys()
        {
            byte[] tempArray = new byte[8];

            for (int i = 0; i < 8; i++) {

                tempArray[i] = this.csp.Key[i];

            }
            Tdes1 = tempArray;

            tdes1 = Encoding.ASCII.GetString(tempArray);


             tempArray = new byte[8];

            for (int i = 0; i < 8; i++)
            {

                tempArray[i] = this.csp.Key[i+8];

            }
            Tdes2 = tempArray;
            this.tdes2 = Encoding.ASCII.GetString(tempArray);

            tempArray = new byte[8];
            for (int i = 0; i < 8; i++)
            {

                tempArray[i] = this.csp.Key[i+16];

            }
            Tdes3 = tempArray;
            this.tdes3 = Encoding.ASCII.GetString(tempArray);

            
        }

        public IList<String> GetKeys()
        {

            var keys = new List<String>
            {
                tdes1,
                tdes2,
                tdes2
            };

            return keys;
        }
    }
}
