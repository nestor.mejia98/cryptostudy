﻿using StructureMap;

namespace TransmisionHandling
{
    public class Bootstrapper : Registry
    {

        // En el constructor de esta clase hacemos uso de IoC e indicamos que para la transmisión
        // se use el tipo Xml. El proyecto está preparado para, en caso de necesitar otro tipo de
        // transmisión como JSON, sólo se cree la clase que implemente esta interfaz y se cambie
        // aquí.
        public Bootstrapper() {

            For<ITransmision>().Use<XmlTransmision>();

        }

    }
}
