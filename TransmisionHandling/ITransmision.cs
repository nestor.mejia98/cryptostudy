﻿using System;
using System.Collections.Generic;
using Entities;

namespace TransmisionHandling
{
    public interface ITransmision
    {

        string WriteOutput(IDataTransmision data);
        IDataTransmision LoadData(String elementToRead);
        IList<string> GetRsaKeys();
        IList<string> GetTdesKeys();
        IList<byte[]> GetSplitTdes();
        string EncryptData(string algorithm, string plainText, string key);
        string DecryptData(string algorithm, string cipherText);
        void SetTdesParameters(byte[] key, byte[] iv);
        string GetPath();

    }
}
