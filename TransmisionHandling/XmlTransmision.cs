﻿using System;
using System.Collections.Generic;
using System.Xml;
using Entities;
using EncryptionService;
using System.IO;

namespace TransmisionHandling
{
    public class XmlTransmision : ITransmision
    {

        public RSAencryptor rsaEncryptor;
        public TDESEncryptor tdesEncryptor;

        public XmlTransmision() {

            rsaEncryptor = new RSAencryptor();
            tdesEncryptor = new TDESEncryptor();

        }

        // Función que carga la data de un archivo devuelve un objeto que implemente la interfaz
        // IDataTransmision (en este caso TdesInfo, RsaInfo o MessageInfo
        public IDataTransmision LoadData(string elementToRead)
        {

            string file = Path.GetFileName(elementToRead);

            string[] noExtension = file.Split(new Char[] {'.'});

            // En base al nombre del archivo, elegimos el método a usar
            switch (noExtension[0])
            {
                case "cp_esclavo":
                    return ReadPublicKey(elementToRead);
                case "tdesencriptado":
                    return ReadTdesKeys(elementToRead);
                case "textoencriptado":
                    return ReadEncryptedMessage(elementToRead);

            }

            return null;
        }

        // Función que recibe un objeto que implemente la interfaz IDataTransmision para escribirlo a Xml
        public string WriteOutput(IDataTransmision data)
        {

            // En función del nombre de la clase que se reciba, se lanza el método necesario
            switch (data.GetType().Name)
            {
                case "TdesInfo":
                    return WriteTdesInfoToXml((TdesInfo)data);
                case "RsaInfo":
                    return WriteRsaInfoToXml((RsaInfo) data);
                case "MessageInfo":
                    return WriteEncryptedMessageToXml((MessageInfo) data);
            }

            return "";
        }

        private string WriteTdesInfoToXml(TdesInfo info) {

            try {

                string tdes1 = info.tdes1;
                string tdes2 = info.tdes2;
                string tdes3 = info.tdes3;
                string iv = info.iv;

                XmlDocument xmlDoc = new XmlDocument();
                XmlNode rootNode = xmlDoc.CreateElement("root");
                xmlDoc.AppendChild(rootNode);

                XmlNode tdes1Node = xmlDoc.CreateElement("tdes1");
                tdes1Node.InnerText = tdes1;

                rootNode.AppendChild(tdes1Node);

                XmlNode tdes2Node = xmlDoc.CreateElement("tdes2");
                tdes2Node.InnerText = tdes2;
                rootNode.AppendChild(tdes2Node);

                XmlNode tdes3Node = xmlDoc.CreateElement("tdes3");
                tdes3Node.InnerText = tdes3;
                rootNode.AppendChild(tdes3Node);

                XmlNode ivNode = xmlDoc.CreateElement("iv");
                ivNode.InnerText = iv;
                rootNode.AppendChild(ivNode);

                using (TextWriter writer = new StreamWriter(GetPath()+"tdesencriptado.xml"))
                {
                    xmlDoc.Save(writer);
                    writer.Close();
                }
                return tdes1+tdes2+tdes3;
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                return "";
            }

        }

        private string WriteRsaInfoToXml(RsaInfo info) {

            try
            {

                string pK = @info.publicKey;

                XmlDocument xmlDoc = new XmlDocument();

                XmlNode root = xmlDoc.CreateElement("root");
                xmlDoc.AppendChild(root);

                XmlNode rootNode = xmlDoc.CreateElement("clavepublica");
                rootNode.InnerText = pK;


                root.AppendChild(rootNode);

                using (TextWriter writer = new StreamWriter(GetPath() + "cp_esclavo.xml"))
                {
                    xmlDoc.Save(writer);
                    writer.Close();
                }
                return pK;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return "";
            }

        }

        private string WriteEncryptedMessageToXml(MessageInfo info) {

            try
            {

                string encryptedText = info.message;

                XmlDocument xmlDoc = new XmlDocument();

                XmlNode root = xmlDoc.CreateElement("root");
                xmlDoc.AppendChild(root);

                XmlNode rootNode = xmlDoc.CreateElement("textoe");
                rootNode.InnerText = encryptedText;

                root.AppendChild(rootNode);

                using (TextWriter writer = new StreamWriter(GetPath() + "textoencriptado.xml"))
                {
                    xmlDoc.Save(writer);
                    writer.Close();
                }
                return encryptedText ;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return "";
            }

        }

        private IDataTransmision ReadPublicKey(string filePath) {

            try
            {

                XmlDocument xml = new XmlDocument();
                xml.Load(filePath);

                var publicKeyNode = xml.GetElementsByTagName("clavepublica");

                string publicKey = publicKeyNode.Item(0).InnerText;


                return new RsaInfo(null, publicKey);

            }
            catch (Exception e) {

                Console.WriteLine(e.Message);

                return null;

            }
            

        }

        private IDataTransmision ReadTdesKeys(string filePath)
        {

            try
            {

                XmlDocument xml = new XmlDocument();
                xml.Load(filePath);

                string tdes1 = xml.GetElementsByTagName("tdes1").Item(0).InnerText;
                string tdes2 = xml.GetElementsByTagName("tdes2").Item(0).InnerText;
                string tdes3 = xml.GetElementsByTagName("tdes3").Item(0).InnerText;
                string iv = xml.GetElementsByTagName("iv").Item(0).InnerText;

                //tdesEncryptor.encryptor.csp.IV = Encoding.Default.GetBytes(rsaEncryptor.DecryptText(iv));


                return new TdesInfo(tdes1, tdes2, tdes3, iv);

            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);

                return null;

            }

        }

        private IDataTransmision ReadEncryptedMessage(string filePath)
        {

            try
            {

                XmlDocument xml = new XmlDocument();
                xml.Load(filePath);

                var messageNode = xml.GetElementsByTagName("textoe");

                string message = messageNode.Item(0).InnerText;


                return new MessageInfo(message);

            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);

                return null;

            }

        }

        public IList<string> GetRsaKeys() {

            List<string> keys = new List<string>
            {
                rsaEncryptor.encryptor.privateKey,
                rsaEncryptor.encryptor.publicKey
            };

            return keys;

        }

        public IList<string> GetTdesKeys() {

            List<string> keys = new List<string>
            {
                tdesEncryptor.encryptor.tdes1,
                tdesEncryptor.encryptor.tdes2,
                tdesEncryptor.encryptor.tdes3
            };

            return keys;

        }

        public IList<byte[]> GetSplitTdes() {

            var SplitTdes = new List<byte[]>
            {
                tdesEncryptor.encryptor.Tdes1,
                tdesEncryptor.encryptor.Tdes2,
                tdesEncryptor.encryptor.Tdes3,
                tdesEncryptor.encryptor.csp.IV,
            };


            return SplitTdes;

        }

        // Función que recupera el path de ejecución para guardar los archivos
        public string GetPath() {
            return System.AppDomain.CurrentDomain.BaseDirectory;
        }

        public void SetTdesParameters(byte[] key, byte[] iv) {

            tdesEncryptor.encryptor.csp.Key = key;
            tdesEncryptor.encryptor.csp.IV = iv;

        }

        // Función que encripta con el algoritmo, texto y clave que se le especifique
        public string EncryptData(string algorithm, string plainText, string key) {

            if (algorithm.Equals("RSA"))
            {

                return rsaEncryptor.EncryptText(plainText, key);

            }
            else if (algorithm.Equals("TDES"))
            {

                return tdesEncryptor.EncryptText(plainText, key);

            }
            else
            {

                return "";

            }

        }

        // Función que desencripta con el algoritmo y el texto que se le indique
        public string DecryptData(string algorithm, string cipherText)
        {

            if (algorithm.Equals("RSA"))
            {

                return rsaEncryptor.DecryptText(cipherText);

            }
            else if (algorithm.Equals("TDES"))
            {

                return tdesEncryptor.DecryptText(cipherText);

            }
            else {

                return "";

            }

            

        }
    }
}
