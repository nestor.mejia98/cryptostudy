﻿using System;
using System.Collections.Generic;
using System.Text;
using StructureMap;
using TransmisionHandling;
using Entities;

namespace Connection
{

    // Clase que maneja la conexión entre el FrontEnd y el Backend, funciona como "una API"
    public class Operation
    {
        // Se lee el contenedor para saber qué clases usar
        public Container container = new Container(c=> { c.AddRegistry<Bootstrapper>(); });
        public ITransmision transmision;

        public Operation() {

            // Se obtiene la instancia especificada en el Bootstraper para la transmisión
             transmision = container.GetInstance<ITransmision>();

        }


        public IList<string> GetRsaKeys() {

            return transmision.GetRsaKeys();

        }

        public string WriteInfoToXml(IDataTransmision infoObject) {

            return transmision.WriteOutput(infoObject);

        }

        public IDictionary<string,object> DecryptTdesInfo(string tdes1, string tdes2, string tdes3, string iv) {

            var DecryptedTdes = new Dictionary<string, object>
            {
                { "HexDecTdes1", transmision.DecryptData("RSA", tdes1) },
                { "HexDecTdes2", transmision.DecryptData("RSA", tdes2) },
                { "HexDecTdes3", transmision.DecryptData("RSA", tdes3) },
                { "HexDecIv", transmision.DecryptData("RSA", iv) }
            };

            var PlainTdes = new Dictionary<string, object>
            {
                { "PlainDecTdes1", Encoding.ASCII.GetString(StringToByteArray((string) DecryptedTdes["HexDecTdes1"])) },
                { "PlainDecTdes2", Encoding.ASCII.GetString(StringToByteArray((string) DecryptedTdes["HexDecTdes2"])) },
                { "PlainDecTdes3", Encoding.ASCII.GetString(StringToByteArray((string) DecryptedTdes["HexDecTdes3"])) }
            };

            AssignTdesInfoToEncryptor(DecryptedTdes);

            return PlainTdes;

        }

        private void AssignTdesInfoToEncryptor(IDictionary<string, object> info) {

            transmision.SetTdesParameters(StringToByteArray((string)info["HexDecTdes1"] + (string)info["HexDecTdes2"] + (string)info["HexDecTdes3"]), StringToByteArray((string) info["HexDecIv"]));

        }

        public string EncryptMessageWithTdes(string plainText, string key) {

            return transmision.EncryptData("TDES", plainText, key);

        }

        public IDataTransmision LoadDataFromXml(string path) {

            return transmision.LoadData(path);

        }

        public IList<string> GetTdesKeys() => transmision.GetTdesKeys();

        public IList<string> GetSplitTdesKeys() {

            var bytes = transmision.GetSplitTdes();

            var hexStrings = new List<string>();

            foreach (var byteArr in bytes ) {

                hexStrings.Add(ByteArrayToString(byteArr));

            }

            return hexStrings;

        }

        public IList<string> EncryptTdesWithRsa(string tdes1, string tdes2, string tdes3, string iv, string pubKey) {

            var encryptedInfo = new List<string>
            {
                transmision.EncryptData("RSA", tdes1, pubKey),
                transmision.EncryptData("RSA", tdes2, pubKey),
                transmision.EncryptData("RSA", tdes3, pubKey),
                transmision.EncryptData("RSA", iv, pubKey)
            };

            return encryptedInfo;

        }

        public string DecryptTextWithTdes(string cipherText) {

            return transmision.DecryptData("TDES", cipherText);
        }

        public string GetRuntimePath() {

            return transmision.GetPath();

        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }

    }
}
